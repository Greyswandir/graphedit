#ifndef GRAPHCONST_H
#define GRAPHCONST_H

#define DRAWNONEMODE        0
#define DRAWDOTMODE         1
#define DRAWLINEMODE        2
#define DRAWRECTMODE        3
#define DRAWCIRCLEMODE      4
#define DRAWELLIPSEMODE     5
#define DRAWPOLYGONMODE     6
#define DRAWPOLYLINEMODE    7
#define DRAWTEXTMODE        8

#define DRAWMODECOUNT       9

#endif // GRAPHCONST_H
