#ifndef MAINVIEW_H
#define MAINVIEW_H

#include <QGraphicsView>
#include <QGraphicsSceneMouseEvent>
#include <QMouseEvent>
#include <QList>
#include <QStatusBar>
#include <QLabel>
#include "figuremodel.h"
#include "drawfigure.h"
#include "fileoptionsdialog.h"

struct CallAction {
    QString name;
    int signal;
};

struct approxParam {
    double a,b;
    double sum_n;
    double max_n;
    int max_ni;
    double minx, maxx;
};

typedef QVector <CallAction> ActionList;

class MainView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit MainView(QWidget *parent = 0);
    void setDrawMode(int mode);
    int drawMode();
    void setOptions(FileOptions opt);
    FileOptions options();

    virtual void mousePressEvent (QMouseEvent *mevent);
    virtual void mouseReleaseEvent (QMouseEvent *mevent);
    virtual void mouseMoveEvent (QMouseEvent *mevent);
    virtual void wheelEvent(QWheelEvent* event);
    void setMainColor(QColor color);
    void setBackColor(QColor color);
    void setSolidFill(int s);
    void setPenSize(int size);
    QColor mainColor();
    QColor backColor();
    int solidFill();
    int penSize();
    QString colorToHex(QColor color);
    FigureModel *figureModel();
    void setStausBar(QStatusBar *bar);
    void setGridSize(int size);
    void setShowGrid(bool show);
    void setSnapToGrid(bool snap);
    QPointF mapCurPos(QPoint pos);
    void updateBoundingRect(SceenItem *item);
    void aproximate();

private:
    void drawDone(QGraphicsItem *item);
    QGraphicsItem *drawCurrentFigure(QPointF p);
    int figureIsDone();
    int figureCanComplete();
    void showMenu(QVector<int> actions, QPoint pos);
    void drawGrid(int flag);
    void drawSelection(int flag);
    void clearDrawing();
    void updateOptions();
    void calcApprox(QVector<QPointF> p, approxParam *param);

    QGraphicsScene *drawScene;
    int dragMode, currentIndex;
    QPointF curPoint, dragPoint, elemStPoint;
    Sceen scene;
    QVector <QGraphicsItem*>  drawedItem;
    SceenItem drawingItem;
    QBrush mainBrush;
    QPen mainPen;
    FigureModel *model;
    ActionList actionList;
    QVector<int> rButtonAction, mButtonAction;
    QVector<int> fButtonAction, bButtonAction;
    QVector<int> tButtonAction, emptyList;
    QVector<int> rItemMenu, mItemMenu;
    QVector<int> fItemMenu, bItemMenu;
    QVector<int> tItemMenu;
    int hasMenu;
    int GridSize;
    bool ShowGrid, SnapToGrid;
    QGraphicsItemGroup *gridElement, *itemSelection;
    QStatusBar *statusBar;
    QLabel *xLbl, *yLbl, *sclLbl;
    DrawFigure *draw;

    qreal scaleFactor, gridPenWidth, snapFactor;
    FileOptions fileOptions;

signals:
    void completeFigure();
    void cancelFigure();
    void continueEdit();
    void figureProperty();
    void figureRemove();

public slots:
    void newAction();
    void saveAction();
    void openAction();
    void completeAction();
    void cancelAction();
    void propertyAction();
    void removeAction();

};

#endif // MAINVIEW_H
