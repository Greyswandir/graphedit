#include "fileoptionsdialog.h"
#include "ui_fileoptionsdialog.h"

FileOptionsDialog::FileOptionsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FileOptionsDialog)
{
    ui->setupUi(this);
    ui->metricValue->addItem("pixel");
    ui->metricValue->addItem("mm");
    ui->metricValue->addItem("inch");
    ui->metricValue->setCurrentIndex(0);
}

FileOptionsDialog::~FileOptionsDialog()
{
    delete ui;
}

void FileOptionsDialog::setOptions (FileOptions opt) {
    fileOptions = opt;
    ui->widthEdit->setText(QString("%1").arg(opt.width));
    ui->heightEdit->setText(QString("%1").arg(opt.height));
    ui->dpiEdit->setText(QString("%1").arg(opt.dpi));
    ui->metricValue->setCurrentIndex(opt.metric);
}

FileOptions FileOptionsDialog::options () {
    qreal w,h,d;
    int m;
    w = ui->widthEdit->text().toDouble();
    h = ui->heightEdit->text().toDouble();
    d = ui->dpiEdit->text().toDouble();
    m = ui->metricValue->currentIndex();
    if (w>0) fileOptions.width = w;
    if (h>0) fileOptions.height = h;
    if (d>0) fileOptions.dpi = d;
    if ((m>=0)&&(m<=2)) fileOptions.metric = m;
    return fileOptions;
}
