#include "drawfigure.h"
#include "graphConst.h"
#include <math.h>

DrawFigure::DrawFigure(QObject *parent) :
    QObject(parent)
{
    drawScene = 0;
    snapFactor = 10;
}

void DrawFigure::setScene(QGraphicsScene *scene) {
    drawScene = scene;
}

void DrawFigure::setSnapFactor(qreal snap) {
    snapFactor = snap;
}

void DrawFigure::drawFigure(SceenItem *item, QPen pen, QBrush brush) {
//QGraphicsItem* DrawFigure::drawFigure(QVector <QPointF> points, QPen pen, QBrush brush,
//    QColor color, int pensize, int drawmode) {
    if (!drawScene) return;
    if (!item) return;
    QRectF r;
    QPolygonF pol;
    if (item->type == DRAWDOTMODE) {
        item->rad = item->pensz/2.0;
        item->item = (QGraphicsItem *)drawScene->addEllipse(item->points[0].x()-item->pensz/2.0, item->points[0].y()-item->pensz/2.0, item->pensz, item->pensz, QPen(item->pen,1), QBrush(item->pen));
    }
    else if (item->type == DRAWTEXTMODE) {
        item->item = (QGraphicsItem*)drawScene->addText(item->text,item->font);
        QFontMetrics fm(item->font);
        QRect r = fm.boundingRect(item->text);
        QPointF pos = item->points[0];
        pos.setX(pos.x()+r.width()-item->item->boundingRect().width());
        pos.setY(pos.y()+r.height()-item->item->boundingRect().height());
        item->item->setPos(pos);
    }
    else {
        if (item->points[0].x() > item->points[1].x()) r.setLeft(item->points[1].x()); else r.setLeft(item->points[0].x());
        if (item->points[0].y() > item->points[1].y()) r.setTop(item->points[1].y()); else r.setTop(item->points[0].y());
        r.setWidth(fabs(item->points[0].x() - item->points[1].x()));
        r.setHeight(fabs(item->points[0].y() - item->points[1].y()));
        if (item->type == DRAWLINEMODE) {
            item->item = (QGraphicsItem *)drawScene->addLine(item->points[0].x(), item->points[0].y(), item->points[1].x(), item->points[1].y(), pen);
        }
        else if (item->type == DRAWRECTMODE) {
            item->item = (QGraphicsItem *)drawScene->addRect(r, pen, brush);
        }
        else if (item->type == DRAWCIRCLEMODE) {
            item->rad = pow(pow(item->points[0].x()-item->points[1].x(),2)+pow(item->points[0].y()-item->points[1].y(),2),0.5);
            item->item = (QGraphicsItem *)drawScene->addEllipse(item->points[0].x()-item->rad, item->points[0].y()-item->rad,2*item->rad,2*item->rad, pen, brush);
        }
        else if (item->type == DRAWELLIPSEMODE) {
            item->item = (QGraphicsItem *)drawScene->addEllipse(r, pen, brush);
        }
        else if (item->type == DRAWPOLYGONMODE) {
            foreach(QPointF pnt, item->points) pol.append(pnt);
            if ((abs(item->points[0].x()-item->points[item->points.count()-1].x())<snapFactor)&&(abs(item->points[0].y()-item->points[item->points.count()-1].y())<snapFactor))
                pol.removeLast();
            item->item = (QGraphicsItem *)drawScene->addPolygon(pol, pen, brush);
        }
        else if (item->type == DRAWPOLYLINEMODE) {
            foreach(QPointF pnt, item->points) pol.append(pnt);
            if ((abs(item->points[0].x()-item->points[item->points.count()-1].x())<snapFactor)&&(abs(item->points[0].y()-item->points[item->points.count()-1].y())<snapFactor)) {
                pol.removeLast();
                item->item = (QGraphicsItem *)drawScene->addPolygon(pol, pen);
            }
            else {
                int i = item->points.count()-2;
                while(i>0) {pol.append(item->points[i]); i--;}
                item->item = (QGraphicsItem *)drawScene->addPolygon(pol, pen);
            }
        }
    }
    if (item->item) item->item->setZValue(1);
}
