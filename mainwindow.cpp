#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QColorDialog>
#include <QDebug>
#include <QSvgRenderer>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);
    readSettings();
    ui->mainColorButton->setIcon(iconFromColor(mainColor));
    ui->backColorButton->setIcon(iconFromColor(backColor));
    ui->mainView->setMainColor(mainColor);
    ui->mainView->setBackColor(backColor);
    ui->mainView->setSolidFill(solid);
    ui->mainView->setGridSize(gridSize);
    ui->mainView->setShowGrid(showGrid != 0);
    ui->mainView->setSnapToGrid(snapToGrid != 0);
    ui->mainView->setStausBar(ui->statusBar);
    ui->mainView->setPenSize(penSize);
    ui->solidButton->setChecked(solid);
    ui->sizeList->setCurrentText(QString("%1").arg(penSize));
    switch (gridSize) {
    case 1: currentGrid = ui->action1_px; break;
    case 2: currentGrid = ui->action2_px; break;
    case 5: currentGrid = ui->action5_px; break;
    case 10: currentGrid = ui->action10_px; break;
    case 15: currentGrid = ui->action15_px; break;
    case 20: currentGrid = ui->action20_px; break;
    case 25: currentGrid = ui->action25_px; break;
    case 50: currentGrid = ui->action50_px; break;
    case 100: currentGrid = ui->action100_px; break;
    case 150: currentGrid = ui->action150_px; break;
    case 200: currentGrid = ui->action200_px; break;
    case 250: currentGrid = ui->action250_px; break;
    case 500: currentGrid = ui->action500_px; break;
    case 1000: currentGrid = ui->action1000_px; break;
    default: currentGrid = ui->action5_px; gridSize = 5; break;
    }
    currentGrid->setChecked(true);
    ui->actionGrid->setChecked(showGrid != 0);
    ui->actionSnap_to_grid->setChecked(snapToGrid != 0);
    connectActions();
    ui->figuresList->setModel(ui->mainView->figureModel());
    ui->splitter->setCollapsible(0,false);
}

MainWindow::~MainWindow() {
    saveSettings();
    delete ui;
}

QIcon MainWindow::iconFromColor(QColor color) {
    return QIcon(emptyPixmap(16, color));
}

QIcon MainWindow::svgIcon(QString name) {
    QSvgRenderer render;
    QPixmap pix = emptyPixmap(16, ui->dotButton->palette().button().color());
    QPainter painter(&pix);
    render.load(name);
    render.render(&painter);
    painter.end();
    return QIcon(pix);
}

QPixmap MainWindow::emptyPixmap(int size, QColor color) {
    QPixmap pix = QPixmap(size,size);
    QPainter painter(&pix);
    painter.setBrush(QBrush(color));
    painter.setPen(QPen(color));
    painter.drawRect(0,0,size,size);
    painter.end();
    return pix;
}

void MainWindow::on_dotButton_clicked() {
    ui->mainView->setDrawMode(DRAWDOTMODE);
}

void MainWindow::on_lineButton_clicked() {
    ui->mainView->setDrawMode(DRAWLINEMODE);
}

void MainWindow::on_squareButton_clicked() {
    ui->mainView->setDrawMode(DRAWRECTMODE);
}

void MainWindow::on_circleButton_clicked() {
    ui->mainView->setDrawMode(DRAWCIRCLEMODE);
}

void MainWindow::on_ellipseButton_clicked()
{
    ui->mainView->setDrawMode(DRAWELLIPSEMODE);
}

void MainWindow::on_polygonButton_clicked() {
    ui->mainView->setDrawMode(DRAWPOLYGONMODE);
}

void MainWindow::on_polilineButton_clicked()
{
    ui->mainView->setDrawMode(DRAWPOLYLINEMODE);
}

void MainWindow::on_textButton_clicked()
{
    ui->mainView->setDrawMode(DRAWTEXTMODE);
}

void MainWindow::on_mainColorButton_clicked() {
    QColor color = QColorDialog::getColor(mainColor);
    if (color.isValid() ) {
        mainColor = color;
        ui->mainColorButton->setIcon(iconFromColor(mainColor));
        ui->mainView->setMainColor(mainColor);
    }
}

void MainWindow::on_backColorButton_clicked() {
    QColor color = QColorDialog::getColor(backColor);
    if (color.isValid() ) {
        backColor = color;
        ui->backColorButton->setIcon(iconFromColor(backColor));
        ui->mainView->setBackColor(backColor);
    }
}

void MainWindow::on_solidButton_clicked() {
    solid = !solid;
    ui->mainView->setSolidFill(solid);
}

void MainWindow::on_sizeList_currentTextChanged(const QString &arg1) {
    int sz = arg1.toInt();
    if (sz > 0) {
        if (penSize != sz) {
          penSize = sz;
          ui->mainView->setPenSize(penSize);
        }
    }
    else {
        if (arg1!="") {
          ui->sizeList->setCurrentText(QString("%1").arg(penSize));
        }
    }
}

void MainWindow::figuresViewToggle() {
    QList <int> list;
    if (ui->actionFigures->isChecked()) {
        list.append(ui->splitter->width()-ui->splitter->handleWidth()-storedWidth);
        list.append(storedWidth);
    }
    else {
        storedWidth = ui->figuresList->width();
        list.append(ui->splitter->width()-ui->splitter->handleWidth());
        list.append(0);
    }
    ui->splitter->setSizes(list);
}

void MainWindow::on_splitter_splitterMoved(int pos, int index) {
    if (ui->splitter->width()-pos < ui->figuresList->width()) {
        if (ui->actionFigures->isChecked()) {
            ui->actionFigures->setChecked(false);
            storedWidth = ui->figuresList->width();
        }
    }
    else {
        if (!ui->actionFigures->isChecked()) {
            ui->actionFigures->setChecked(true);
        }
    }
}

void MainWindow::gridChanged() {
    QObject *obj = QObject::sender();
    if (QAction *act=qobject_cast<QAction *>(obj)) {
        if (currentGrid != act) {
            currentGrid->setChecked(false);
            QString str = act->text();
            str.remove(str.length()-3,3);
            gridSize = str.toInt();
            ui->mainView->setGridSize(gridSize);
        }
        currentGrid = act;
    }
}

void MainWindow::gridShow() {
    if (ui->actionGrid->isChecked()) showGrid = 1;
    else showGrid = 0;
    ui->mainView->setShowGrid(showGrid != 0);
}

void MainWindow::gridSnap() {
    if (ui->actionSnap_to_grid->isChecked()) snapToGrid = 1;
    else snapToGrid = 0;
    ui->mainView->setSnapToGrid(snapToGrid != 0);
}

void MainWindow::fileOptions() {
    FileOptionsDialog *dlg = new FileOptionsDialog(this);
    dlg->setOptions(ui->mainView->options());
    if (dlg->exec()) ui->mainView->setOptions(dlg->options());
    delete dlg;
}

void MainWindow::readSettings() {
    mainColor.setRgb(0,0,0);
    backColor.setRgb(255,255,255);
    solid = 0;
    penSize = 2;
    gridSize = 5;
    showGrid = 0;
    snapToGrid = 0;
    QSettings setting("WizardTech", "GraphEdit");
    setting.sync();
    mainColor = storedColor(&setting, "PenColor", mainColor);
    backColor = storedColor(&setting, "BrushColor", backColor);
    solid = storedParam (&setting, "Solid", solid);
    penSize = storedParam (&setting, "PenSize", penSize);
    if (penSize <= 0) penSize = 2;
    gridSize = storedParam (&setting, "GridSize", penSize);
    if (gridSize < 1) gridSize = 1;
    showGrid = storedParam (&setting, "ShowGrid", showGrid);
    snapToGrid = storedParam (&setting, "SnapToGrid", snapToGrid);
    setting.sync();
}

int MainWindow::storedParam(QSettings *setting, QString name, int def) {
    QVariant v = setting->value(name);
    int value = def;
    if (v.isValid()) value = v.toString().toInt();
    else setting->setValue(name, QString("%1").arg(def));
    return value;
}

QColor MainWindow::storedColor(QSettings *setting, QString name, QColor def) {
    QVariant v = setting->value(name);
    QColor value = def;
    QString s;
    if (v.isValid()) {
        s = v.toString();
        value.setNamedColor(s);
    }
    else setting->setValue(name, ui->mainView->colorToHex(def));
    return value;
}

void MainWindow::saveSettings() {
    QSettings setting("WizardTech", "GraphEdit");
    setting.setValue("PenColor", ui->mainView->colorToHex(mainColor));
    setting.setValue("BrushColor", ui->mainView->colorToHex(backColor));
    setting.setValue("Solid", QString("%1").arg(solid));
    setting.setValue("PenSize", QString("%1").arg(penSize));
    setting.setValue("GridSize", QString("%1").arg(gridSize));
    setting.setValue("ShowGrid", QString("%1").arg(showGrid));
    setting.setValue("SnapToGrid", QString("%1").arg(snapToGrid));
    setting.sync();
}

void MainWindow::connectActions() {
    connect(ui->action_New,SIGNAL(triggered()),ui->mainView,SLOT(newAction()));
    connect(ui->action_Save,SIGNAL(triggered()),ui->mainView,SLOT(saveAction()));
    connect(ui->action_Open,SIGNAL(triggered()),ui->mainView,SLOT(openAction()));
    connect(ui->actionOptions_2,SIGNAL(triggered()),ui->mainView,SLOT(propertyAction()));
    connect(ui->actionFigures,SIGNAL(triggered()),this,SLOT(figuresViewToggle()));
    connect(ui->action1_px,SIGNAL(triggered()),this,SLOT(gridChanged()));
    connect(ui->action2_px,SIGNAL(triggered()),this,SLOT(gridChanged()));
    connect(ui->action5_px,SIGNAL(triggered()),this,SLOT(gridChanged()));
    connect(ui->action10_px,SIGNAL(triggered()),this,SLOT(gridChanged()));
    connect(ui->action15_px,SIGNAL(triggered()),this,SLOT(gridChanged()));
    connect(ui->action20_px,SIGNAL(triggered()),this,SLOT(gridChanged()));
    connect(ui->action25_px,SIGNAL(triggered()),this,SLOT(gridChanged()));
    connect(ui->action50_px,SIGNAL(triggered()),this,SLOT(gridChanged()));
    connect(ui->action100_px,SIGNAL(triggered()),this,SLOT(gridChanged()));
    connect(ui->action150_px,SIGNAL(triggered()),this,SLOT(gridChanged()));
    connect(ui->action200_px,SIGNAL(triggered()),this,SLOT(gridChanged()));
    connect(ui->action250_px,SIGNAL(triggered()),this,SLOT(gridChanged()));
    connect(ui->action500_px,SIGNAL(triggered()),this,SLOT(gridChanged()));
    connect(ui->action1000_px,SIGNAL(triggered()),this,SLOT(gridChanged()));
    connect(ui->actionGrid,SIGNAL(triggered()),this,SLOT(gridShow()));
    connect(ui->actionSnap_to_grid,SIGNAL(triggered()),this,SLOT(gridSnap()));
    connect(ui->actionFile_options,SIGNAL(triggered()),this,SLOT(fileOptions()));
}

void MainWindow::on_aproxButton_clicked()
{
    ui->mainView->aproximate();
}
