#ifndef FIGUREMODEL_H
#define FIGUREMODEL_H

#include <QGraphicsView>
#include <QAbstractTableModel>
#include <QVector>
#include "graphConst.h"

struct SceenItem {
    QGraphicsItem *item;
    QVector <QPointF> points;
    int type;
    QColor pen, brush;
    int solid, pensz;
    qreal rad;
    QString name;
    QRectF bounding;
    QFont font;
    QString text;
    QPointF stPos;
};

struct Sceen {
    QVector <SceenItem> list;
};

struct SceenTreeItem {
    QVector <SceenTreeItem*> child;
    QString text;
    int row;
    SceenTreeItem* parent;
};

class FigureModel : public QAbstractTableModel
{
    Q_OBJECT
    QStringList headers;
    Sceen *sceen;
    SceenItem storedFigure;
    QVector <int> figureIndexes;
    QVector <QString> figureNames;
    SceenTreeItem *rootItem;
public:
    explicit FigureModel(QObject *parent = 0);

    void setMainSceen (Sceen *scn);
    Sceen *mainSceen();
    void addFigure(SceenItem itm, bool createName = true);
    void removeItem(int i);
    void clearSceen();
    void moveFigure(int i, QPointF mv);
    void storePosition(int i);
    void updateFigureData(int i);

    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);

    int rowCount(const QModelIndex &parent=QModelIndex()) const;
    int columnCount(const QModelIndex &parent) const;

    QVariant headerData(int section,Qt::Orientation orientation, int role=Qt::DisplayRole) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    QModelIndex index(int row, int column, const QModelIndex &parent) const;
    QModelIndex parent(const QModelIndex &index) const;

signals:

public slots:

private:
    void updateTreeData (int i);
};

#endif // FIGUREMODEL_H
