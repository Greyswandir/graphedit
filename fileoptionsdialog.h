#ifndef FILEOPTIONSDIALOG_H
#define FILEOPTIONSDIALOG_H

#include <QDialog>

struct FileOptions {
    qreal width;
    qreal height;
    qreal dpi;
    int metric;
};

namespace Ui {
class FileOptionsDialog;
}

class FileOptionsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FileOptionsDialog(QWidget *parent = 0);
    ~FileOptionsDialog();
    void setOptions (FileOptions opt);
    FileOptions options ();

private:
    Ui::FileOptionsDialog *ui;
    FileOptions fileOptions;
};

#endif // FILEOPTIONSDIALOG_H
