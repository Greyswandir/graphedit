#include "figureproperties.h"
#include "ui_figureproperties.h"
#include <QColorDialog>
#include <QDebug>

FigureProperties::FigureProperties(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FigureProperties)
{
    ui->setupUi(this);
    mainScene = 0;
    draw = new DrawFigure();
    mainFigure = 0;
}

FigureProperties::~FigureProperties() {
    delete ui;
    delete draw;
}

QIcon FigureProperties::iconFromColor(QColor color) {
    QPixmap pix = QPixmap(16,16);
    QPainter painter(&pix);
    painter.setBrush(QBrush(color));
    painter.setPen(QPen(color));
    painter.drawRect(0,0,16,16);
    painter.end();
    QIcon icon = QIcon(pix);
    return icon;
}

void FigureProperties::drawFigure() {
    if (sceneFigure.item) {
        mainScene->removeItem(sceneFigure.item);
        delete sceneFigure.item;
        sceneFigure.item = 0;
    }
    QBrush brush;
    if (sceneFigure.solid) brush = QBrush(sceneFigure.brush);
    else brush = QBrush();
    draw->drawFigure(&sceneFigure,QPen(sceneFigure.pen,sceneFigure.pensz),brush);
}

void FigureProperties::setFigure(SceenItem itm) {
    sceneFigure = itm;
    sceneFigure.item = 0;
    mainScene = new QGraphicsScene(sceneFigure.bounding, ui->graphicsView);
    ui->graphicsView->setScene(mainScene);
    draw->setScene(mainScene);
    drawFigure();
    ui->mainColorButton->setIcon(iconFromColor(sceneFigure.pen));
    ui->backColorButton->setIcon(iconFromColor(sceneFigure.brush));
    ui->solidButton->setChecked(sceneFigure.solid);
    ui->sizeList->setCurrentText(QString("%1").arg(sceneFigure.pensz));
    ui->xCoord->setText(QString("%1").arg(sceneFigure.bounding.x()));
    ui->yCoord->setText(QString("%1").arg(sceneFigure.bounding.y()));
    ui->figureName->setText(sceneFigure.name);
}

SceenItem FigureProperties::figure() {
    return sceneFigure;
}

void FigureProperties::on_solidButton_clicked() {
    sceneFigure.solid = !sceneFigure.solid;
    drawFigure();
}

void FigureProperties::on_mainColorButton_clicked() {
    QColor color = QColorDialog::getColor(sceneFigure.pen);
    if (color.isValid() ) {
        sceneFigure.pen = color;
        ui->mainColorButton->setIcon(iconFromColor(sceneFigure.pen));
        drawFigure();
    }
}

void FigureProperties::on_backColorButton_clicked() {
    QColor color = QColorDialog::getColor(sceneFigure.brush);
    if (color.isValid() ) {
        sceneFigure.brush = color;
        ui->backColorButton->setIcon(iconFromColor(sceneFigure.brush));
        drawFigure();
    }
}

void FigureProperties::on_sizeList_currentTextChanged(const QString &arg1) {
    int sz = arg1.toInt();
    if (sz > 0) {
        if (sceneFigure.pensz != sz) {
            sceneFigure.pensz = sz;
            drawFigure();
        }
    }
    else {
        if (arg1!="") {
          ui->sizeList->setCurrentText(QString("%1").arg(sceneFigure.pensz));
        }
    }
}

void FigureProperties::on_figureName_textChanged(const QString &arg1) {
    sceneFigure.name = arg1;
}
