#ifndef DRAWFIGURE_H
#define DRAWFIGURE_H

#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include "figuremodel.h"

class DrawFigure : public QObject
{
    Q_OBJECT
public:
    explicit DrawFigure(QObject *parent = 0);
    void setScene(QGraphicsScene *scene);
    void drawFigure(SceenItem *item, QPen pen, QBrush brush);
    void setSnapFactor(qreal snap);

signals:

public slots:

private:
    QGraphicsScene *drawScene;
    int radius;
    qreal snapFactor;

};

#endif // DRAWFIGURE_H
