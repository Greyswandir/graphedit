#include "textdialog.h"
#include "ui_textdialog.h"
#include <QFontDialog>

TextDialog::TextDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TextDialog)
{
    ui->setupUi(this);
    curFont = ui->textEdit->currentFont();
}

TextDialog::~TextDialog()
{
    delete ui;
}

QString TextDialog::enteredText() {
    return ui->textEdit->toPlainText();
}

void TextDialog::on_fontButton_clicked()
{
    bool ok;
    QFontDialog dlg;
    dlg.setFont(curFont);
    QFont fnt = dlg.getFont(&ok, curFont);
    if (ok) {
        curFont = fnt;
        ui->textEdit->setCurrentFont(curFont);
    }
}

QFont TextDialog::currentFnt() {
    return curFont;
}
