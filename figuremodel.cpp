#include "figuremodel.h"
#include <QGraphicsItem>
#include <QDebug>

FigureModel::FigureModel(QObject *parent) :
    QAbstractTableModel(parent)
{
    headers << tr("Figure");
    for (int i = 0; i < DRAWMODECOUNT; i++) figureIndexes.append(0);
    figureNames << "" << "dot" << "line" << "rect" << "circle" << "ellipse" << "polygon" << "poliline" << "text";
    sceen = 0;
    rootItem = new SceenTreeItem[1];
    rootItem->child.clear();
    rootItem->row = 0;
    rootItem->text = "";
    rootItem->parent = 0;
}

void FigureModel::setMainSceen (Sceen *scn) {
    sceen = scn;
}

Sceen* FigureModel::mainSceen() {
    return sceen;
}

void FigureModel::updateTreeData (int i) {
    if (sceen&&rootItem) {
        if (i < rootItem->child.count()) {
            rootItem->child[i]->text = sceen->list[i].name;
            rootItem->child[i]->row = i;
            rootItem->child[i]->child[0]->text = figureNames[sceen->list[i].type];
            rootItem->child[i]->child[0]->row = 0;
            rootItem->child[i]->child[0]->parent = rootItem->child[i];
            rootItem->child[i]->child[1]->text = QString("Pen size: %1").arg(sceen->list[i].pensz);
            rootItem->child[i]->child[1]->row = 1;
            rootItem->child[i]->child[1]->parent = rootItem->child[i];
            rootItem->child[i]->child[2]->text = QString("Solid: %1").arg(sceen->list[i].solid);
            rootItem->child[i]->child[2]->row = 2;
            rootItem->child[i]->child[2]->parent = rootItem->child[i];
            rootItem->child[i]->child[3]->text = QString("Pen Color: #%1").arg(sceen->list[i].pen.rgba()&0xFFFFFF, 0, 16);
            rootItem->child[i]->child[3]->row = 3;
            rootItem->child[i]->child[3]->parent = rootItem->child[i];
            rootItem->child[i]->child[4]->text = QString("Brush Color: #%1").arg(sceen->list[i].brush.rgba()&0xFFFFFF, 0, 16);
            rootItem->child[i]->child[4]->row = 4;
            rootItem->child[i]->child[4]->parent = rootItem->child[i];
            rootItem->child[i]->child[5]->text = QString("Rect (x=%1,y=%2; w=%3,h=%4").arg(sceen->list[i].bounding.x()).arg(sceen->list[i].bounding.y()).arg(sceen->list[i].bounding.width()).arg(sceen->list[i].bounding.height());
            rootItem->child[i]->child[5]->row = 5;
            rootItem->child[i]->child[5]->parent = rootItem->child[i];
        }
    }
}

void FigureModel::addFigure(SceenItem itm, bool createName) {
    SceenTreeItem *it;
    if(sceen) {
        beginInsertRows(QModelIndex(), sceen->list.count(),sceen->list.count());
        if (createName) itm.name = figureNames[itm.type] + QString("_%1").arg(figureIndexes[itm.type]++);
        sceen->list.append(itm);
        it = new SceenTreeItem[1];
        it->child.clear();
        it->parent = rootItem;
        it->child.append(new SceenTreeItem[1]);
        it->child.append(new SceenTreeItem[1]);
        it->child.append(new SceenTreeItem[1]);
        it->child.append(new SceenTreeItem[1]);
        it->child.append(new SceenTreeItem[1]);
        it->child.append(new SceenTreeItem[1]);
        rootItem->child.append(it);
        updateTreeData (sceen->list.count()-1);
        endInsertRows();
    }
}

void FigureModel::moveFigure(int i, QPointF mv) {
    sceen->list[i].item->setPos(sceen->list[i].stPos+mv);
    sceen->list[i].bounding.moveTopLeft(storedFigure.bounding.topLeft()+mv);
    for (int j = 0; j < sceen->list[i].points.count(); j++) {
        sceen->list[i].points[j] = storedFigure.points[j] + mv;
    }
    updateTreeData (i);
    //emit dataChanged(QModelIndex(),QModelIndex());
    emit dataChanged(index(i,0,QModelIndex()),index(i,headers.count(),QModelIndex()));
}

void FigureModel::storePosition(int i) {
    storedFigure = sceen->list[i];
}

void FigureModel::updateFigureData(int i) {
    updateTreeData (i);
    emit dataChanged(index(i,0,QModelIndex()),index(i,headers.count(),QModelIndex()));
}

void FigureModel::removeItem(int i) {
    if (sceen) {
        beginRemoveRows(QModelIndex(), i, i);
        sceen->list.remove(i);
        rootItem->child.remove(i);
        for(int i = 0; i < rootItem->child.count(); i++) {
            rootItem->child[i]->row = i;
        }
        endRemoveRows();
    }
}

void FigureModel::clearSceen() {
    if(sceen&&(sceen->list.count()>0)) {
        beginRemoveRows(QModelIndex(), 0, sceen->list.count()-1);
        sceen->list.clear();
        rootItem->child.clear();
        for (int i = 0; i < DRAWMODECOUNT; i++) figureIndexes[i]=0;
        endRemoveRows();
    }
}

QVariant FigureModel::data(const QModelIndex &index, int role) const {
    if (index.isValid()&&(index.internalPointer())){
        if (role == Qt::DisplayRole || role == Qt::EditRole) {
            SceenTreeItem* it = static_cast<SceenTreeItem*>(index.internalPointer());
            return it->text;
        }
    }
    return QVariant();
}

bool FigureModel::setData(const QModelIndex &index, const QVariant &value, int role) {
    return false;
}

int FigureModel::rowCount(const QModelIndex &parent) const {
    SceenTreeItem *parentItem;
    if (parent.column() > 0) return 0;
    if (!parent.isValid()) parentItem = rootItem;
    else parentItem = static_cast<SceenTreeItem*>(parent.internalPointer());
    return parentItem->child.count();
}

int FigureModel::columnCount(const QModelIndex &parent) const {
    //if (parent != QModelIndex()) return 0;
    return headers.count();
}

QVariant FigureModel::headerData(int section,Qt::Orientation orientation, int role) const {
    if(role != Qt::DisplayRole)
               return QVariant();
    if(orientation == Qt::Horizontal && role == Qt::DisplayRole){
       return headers.at(section); // заголовки столбцов
    }else{
       return QString("%1").arg( section + 1 ); // возвращаем номера строк
    }
}

Qt::ItemFlags FigureModel::flags(const QModelIndex &index) const {
    if (!index.isValid())
            return Qt::ItemIsEnabled;
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

QModelIndex FigureModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    SceenTreeItem *parentItem;


    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<SceenTreeItem*>(parent.internalPointer());

    if (parentItem->child.count()>row)
        return createIndex(row, column, parentItem->child[row]);
    else
        return QModelIndex();
}

QModelIndex FigureModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    SceenTreeItem *childItem = static_cast<SceenTreeItem*>(index.internalPointer());
    SceenTreeItem *parentItem = childItem->parent;

    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->row, 0, parentItem);
}
