#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include "mainview.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QIcon iconFromColor(QColor color);
    QIcon svgIcon(QString name);
    QPixmap emptyPixmap(int size, QColor color);
    void readSettings();
    void saveSettings();
    void connectActions();
    int storedParam(QSettings *setting, QString name, int def);
    QColor storedColor(QSettings *setting, QString name, QColor def);

private slots:
    void on_dotButton_clicked();
    void on_lineButton_clicked();
    void on_squareButton_clicked();
    void on_circleButton_clicked();
    void on_polygonButton_clicked();
    void on_mainColorButton_clicked();
    void on_backColorButton_clicked();
    void on_solidButton_clicked();
    void on_sizeList_currentTextChanged(const QString &arg1);
    void figuresViewToggle();
    void on_splitter_splitterMoved(int pos, int index);
    void on_ellipseButton_clicked();
    void on_polilineButton_clicked();
    void on_textButton_clicked();
    void gridChanged();
    void gridShow();
    void gridSnap();
    void fileOptions();

    void on_aproxButton_clicked();

private:
    Ui::MainWindow *ui;
    int drawMode;
    QColor mainColor, backColor;
    QAction *currentGrid;
    int solid;
    int penSize;
    int storedWidth;
    int gridSize;
    int showGrid, snapToGrid;
};

#endif // MAINWINDOW_H
