#include "mainview.h"
#include <QDebug>
#include <math.h>
#include <QFileDialog>
#include <QDomDocument>
#include <QMenu>
#include <textdialog.h>
#include <QGraphicsItem>
#include "figureproperties.h"

MainView::MainView(QWidget *parent) :
    QGraphicsView(parent)
{
    drawScene = new QGraphicsScene();
    setScene(drawScene);
    drawScene->setSceneRect(0,0,1024,768);
    fileOptions.width=1024;
    fileOptions.height=768;
    fileOptions.dpi=300;
    fileOptions.metric=0;
    drawingItem.type = DRAWNONEMODE;
    drawingItem.points.clear();
    drawingItem.pen.setRgb(0,0,0);
    drawingItem.brush.setRgb(255,255,255);
    drawingItem.solid = 0;
    drawingItem.pensz = 0;
    drawingItem.item = 0;
    snapFactor = 10;
    scene.list.clear();
    itemSelection = 0;
    gridElement = 0;
    dragMode = 0;
    currentIndex = -1;
    mainBrush = QBrush();
    mainPen = QPen(drawingItem.pen, drawingItem.pensz);
    model = new FigureModel(parent);
    model->setMainSceen(&scene);
    setMouseTracking(true);
    connect(this,SIGNAL(completeFigure()),this,SLOT(completeAction()));
    connect(this,SIGNAL(cancelFigure()),this,SLOT(cancelAction()));
    connect(this,SIGNAL(figureProperty()),this,SLOT(propertyAction()));
    connect(this,SIGNAL(figureRemove()),this,SLOT(removeAction()));
    actionList.clear();
    CallAction act;
    act.name = tr("Complete figure");
    act.signal = 0;
    actionList.append(act);
    act.name = tr("Cancel figure");
    act.signal = 1;
    actionList.append(act);
    act.name = tr("Continue editing");
    act.signal = 2;
    actionList.append(act);
    act.name = tr("Figure properties");
    act.signal = 3;
    actionList.append(act);
    act.name = tr("Remove figure");
    act.signal = 4;
    actionList.append(act);
    hasMenu = 0;
    GridSize = 5;
    ShowGrid = false;
    SnapToGrid = false;
    rButtonAction.clear();
    mButtonAction.clear();
    fButtonAction.clear();
    bButtonAction.clear();
    tButtonAction.clear();
    rButtonAction.append(0);
    rButtonAction.append(1);
    rButtonAction.append(2);
    mButtonAction.append(0);
    fButtonAction.append(1);
    bButtonAction.append(2);
    rItemMenu.clear();
    rItemMenu.append(3);
    rItemMenu.append(4);
    emptyList.clear();
    statusBar = 0;
    draw = new DrawFigure();
    draw->setScene(drawScene);
    scaleFactor = 1;
    gridPenWidth = 1;
}

void MainView::setOptions(FileOptions opt) {
    fileOptions = opt;
    updateOptions();
}

void MainView::updateOptions() {
    if (itemSelection) {
        drawScene->removeItem(itemSelection);
        delete itemSelection;
        itemSelection = 0;
    }
    if (ShowGrid) drawGrid(1);
    drawScene->setSceneRect(0,0,fileOptions.width,fileOptions.height);
    if (ShowGrid) drawGrid(2);
}

FileOptions MainView::options() {
    return fileOptions;
}

void MainView::setStausBar(QStatusBar *bar) {
    statusBar = bar;
    xLbl = new QLabel();
    yLbl = new QLabel();
    sclLbl = new QLabel();
    xLbl->setText(tr("X = "));
    yLbl->setText(tr("Y = "));
    sclLbl->setText(tr("Scale = 100%"));
    xLbl->setFrameShape(QFrame::Panel);
    xLbl->setFrameShadow(QFrame::Sunken);
    xLbl->setMaximumSize(100,xLbl->maximumSize().height());
    xLbl->setMinimumSize(100,xLbl->minimumSize().height());
    yLbl->setFrameShape(QFrame::Panel);
    yLbl->setFrameShadow(QFrame::Sunken);
    yLbl->setMaximumSize(100,yLbl->maximumSize().height());
    yLbl->setMinimumSize(100,yLbl->minimumSize().height());
    sclLbl->setFrameShape(QFrame::Panel);
    sclLbl->setFrameShadow(QFrame::Sunken);
    sclLbl->setMaximumSize(100,sclLbl->maximumSize().height());
    sclLbl->setMinimumSize(100,sclLbl->minimumSize().height());
    statusBar->addWidget(xLbl);
    statusBar->addWidget(yLbl);
    statusBar->addWidget(sclLbl);
}

void MainView::setDrawMode(int mode) {
    drawingItem.type = mode;
    clearDrawing();
    drawingItem.points.clear();
    drawingItem.rad = 0;
    drawingItem.font = QFont();
    drawingItem.item = 0;
}

int MainView::drawMode() {
    return drawingItem.type;
}

FigureModel* MainView::figureModel() {
    return model;
}

void MainView::mousePressEvent (QMouseEvent *mevent){
    QPointF pos = mapCurPos(mevent->pos());
    if (SnapToGrid) {
        qreal x = pos.x(), y = pos.y();
        x = floor(x/GridSize+0.5)*GridSize;
        y = floor(y/GridSize+0.5)*GridSize;
        pos.setX(x); pos.setY(y);
    }
    curPoint = pos;
    if (mevent->button() == Qt::LeftButton) {
        if (drawingItem.type != DRAWNONEMODE) {
            if (drawingItem.type == DRAWDOTMODE) {
                drawingItem.points.append(pos);
                drawDone(drawCurrentFigure(pos));
            }
            else if (drawingItem.type == DRAWTEXTMODE) {
                TextDialog *dlg = new TextDialog(this);
                if (dlg->exec()) {
                    drawingItem.font = dlg->currentFnt();
                    drawingItem.text = dlg->enteredText();
                    QGraphicsItem *it = drawCurrentFigure(pos);
                    drawingItem.points.append(pos);
                    drawDone(it);
                }
                delete dlg;
            }
            else {
                if (drawingItem.points.count()) {
                    QGraphicsItem *it = drawCurrentFigure(pos);
                    drawingItem.points.append(pos);
                    if (figureIsDone()) drawDone(it);
                }
                else drawingItem.points.append(pos);
            }
        }
        else {
            QGraphicsItem *item;
            QList<QGraphicsItem *> itms = items(mevent->pos());
            int index = -1, i = 0, j = -1, drag = 1;
            j = itms.indexOf(gridElement);
            if (ShowGrid&&(j >= 0)) itms.removeAt(j);
            if (itms.indexOf(itemSelection) >= 0) {
                dragMode = 1;
                dragPoint = curPoint;
                elemStPoint = itemSelection->pos();
                scene.list[currentIndex].stPos = scene.list[currentIndex].item->pos();
                model->storePosition(currentIndex);
            }
            else {
                if (itms.count() > 0) {
                    if (itms.count()>1) {
                        QMenu menu;
                        QAction **action, *selAction;
                        action = new QAction*[itms.count()];
                        foreach (item, itms) {
                            index = drawedItem.indexOf(item);
                            if (index != -1) {
                                action[i] = new QAction(scene.list.at(index).name, this);
                                action[i]->setProperty("ItemIndex", index);
                                menu.addAction(action[i]);
                                i++;
                            }
                        }
                        j = i;
                        if (j > 1) {
                            hasMenu = 1;
                            drag = 0;
                            selAction = menu.exec(mapToGlobal(mevent->pos()));
                            if (selAction) index = selAction->property("ItemIndex").toInt();
                            else index = -1;
                        }
                        else if (j == 1) index = action[0]->property("ItemIndex").toInt();
                        for (i = 0; i < j; i++) delete action[i];
                        delete action;
                        hasMenu = 0;
                    }
                    else {
                        item = itms.at(0);
                        index = drawedItem.indexOf(item);
                    }
                    if (index != -1) {
                        currentIndex = index;
                        dragMode = drag;
                        drawSelection(1);
                        dragPoint = curPoint;
                        elemStPoint = itemSelection->pos();
                        scene.list[index].stPos = scene.list[index].item->pos();
                        model->storePosition(index);
                    }
                }
            }
        }
    }
    else if (mevent->button() == Qt::RightButton) {
        if (drawingItem.type != DRAWNONEMODE) showMenu(rButtonAction, mapToGlobal(mevent->pos()));
        QList<QGraphicsItem *> itms = items(mevent->pos());
        if (itemSelection) {
            if (itms.indexOf(itemSelection) >= 0) {
                showMenu(rItemMenu, mapToGlobal(mevent->pos()));
            }
            else {
                drawScene->removeItem(itemSelection);
                delete itemSelection;
                itemSelection = 0;
                if (dragMode) dragMode = 0;
                currentIndex = -1;
            }
        }
    }
    else if (mevent->button() == Qt::MiddleButton) {
        if (drawingItem.type != DRAWNONEMODE) showMenu(mButtonAction, mapToGlobal(mevent->pos()));
    }
    else if (mevent->button() == Qt::BackButton) {
        if (drawingItem.type != DRAWNONEMODE) showMenu(bButtonAction, mapToGlobal(mevent->pos()));
    }
    else if (mevent->button() == Qt::ForwardButton) {
        if (drawingItem.type != DRAWNONEMODE) showMenu(fButtonAction, mapToGlobal(mevent->pos()));
    }
    else if (mevent->button() == Qt::TaskButton) {
        if (drawingItem.type != DRAWNONEMODE) showMenu(tButtonAction, mapToGlobal(mevent->pos()));
    }
}

void MainView::mouseReleaseEvent (QMouseEvent *mevent) {
    if (mevent->button() == Qt::LeftButton) {
        if (dragMode) dragMode = 0;
    }
}

void MainView::mouseMoveEvent (QMouseEvent *mevent) {
    QPointF pos = mapCurPos(mevent->pos());
    if (statusBar) {
        xLbl->setText(tr("X = %1").arg(pos.x(),3, 'g', -1,'0'));
        yLbl->setText(tr("Y = %1").arg(pos.y(),3, 'g', -1,'0'));
    }
    if ((drawingItem.type != DRAWNONEMODE)&&(drawingItem.points.count())) {
        drawCurrentFigure(pos);
    }
    if (dragMode) {
        QPointF p = pos-dragPoint;
        itemSelection->setPos(elemStPoint+p);
        model->moveFigure(currentIndex, p);
    }
}
void MainView::wheelEvent(QWheelEvent* event) {
    qreal scaling;
    if (event->modifiers() & Qt::ControlModifier) {
        if (event->delta()>0) scaling = 2;
        else scaling = 0.5;
        scaleFactor *= scaling;
        if ((scaleFactor >= 0.03125)&&(scaleFactor <=64)) {
            scale(scaling, scaling);
            gridPenWidth = 1/scaleFactor;
            drawGrid(3);
            drawSelection(0);
        }
        else scaleFactor /= scaling;
        snapFactor = 10/scaleFactor;
        sclLbl->setText(tr("Scale = %1%").arg(scaleFactor*100));
    }
    else {
        QGraphicsView::wheelEvent(event);
    }
}

QPointF MainView::mapCurPos(QPoint pos) {
    QPointF p = mapToScene(pos);
    if (p.x()<0) p.setX(0);
    if (p.y()<0) p.setY(0);
    if (SnapToGrid) {
        p.setX(floor(p.x()/GridSize+0.5)*GridSize);
        p.setY(floor(p.y()/GridSize+0.5)*GridSize);
    }
    if (p.x()>drawScene->width()-1) p.setX(drawScene->width()-1);
    if (p.y()>drawScene->height()-1) p.setY(drawScene->height()-1);
    return p;
}

void MainView::setMainColor(QColor color) {
    drawingItem.pen = color;
    mainPen = QPen(drawingItem.pen, drawingItem.pensz);
}

void MainView::setBackColor(QColor color) {
    drawingItem.brush = color;
    if (drawingItem.solid) mainBrush = QBrush(drawingItem.brush);
}

void MainView::setSolidFill(int s) {
    drawingItem.solid = s;
    if (drawingItem.solid) mainBrush = QBrush(drawingItem.brush);
    else mainBrush = QBrush();
}

void MainView::setPenSize(int size) {
    drawingItem.pensz = size;
    mainPen = QPen(drawingItem.pen, drawingItem.pensz);
}

QColor MainView::mainColor() {
    return drawingItem.pen;
}

QColor MainView::backColor() {
    return drawingItem.brush;
}

int MainView::solidFill() {
    return drawingItem.solid;
}

int MainView::penSize() {
    return drawingItem.pensz;
}

void MainView::drawDone(QGraphicsItem *item) {
    SceenItem itm;
    if (item) {
        itm = drawingItem;
        itm.item = item;
        if (itm.type == DRAWPOLYGONMODE) itm.points.removeLast();
        else if (itm.type == DRAWPOLYLINEMODE) {
            if (figureIsDone()==1) {
                itm.points[itm.points.count()-1].setX(itm.points[0].x());
                itm.points[itm.points.count()-1].setY(itm.points[0].y());
            }
            else itm.points.removeLast();
            itm.solid = 0;
        }
        if (itm.type == DRAWTEXTMODE) itm.solid = 0;
        updateBoundingRect(&itm);
        model->addFigure(itm);
        drawedItem.append(item);
    }
    drawingItem.item = 0;
    drawingItem.type = DRAWNONEMODE;
    drawingItem.points.clear();
}

void MainView::updateBoundingRect(SceenItem *item) {
    qreal l = 0, t = 0, w = 0, h = 0;
    if (item) {
        if ((item->type == DRAWPOLYGONMODE)||(item->type == DRAWPOLYLINEMODE)) {
            QPointF min, max;
            max = min = QPointF(item->points[0].x(), item->points[0].y());
            foreach(QPointF p, item->points) {
                if (p.x()<min.x()) min.setX(p.x());
                if (p.y()<min.y()) min.setY(p.y());
                if (p.x()>max.x()) max.setX(p.x());
                if (p.y()>max.y()) max.setY(p.y());
            }
            l = min.x(); t = min.y();
            w = max.x() - min.x(); h = max.y() - min.y();
        }
        if (item->type == DRAWLINEMODE) {
            item->solid = 0;
            if (item->points[0].x() > item->points[1].x()) l = item->points[1].x(); else l = item->points[0].x();
            if (item->points[0].y() > item->points[1].y()) t = item->points[1].y(); else t = item->points[0].y();
            w = fabs(item->points[1].x()-item->points[0].x()); h = fabs(item->points[1].y()-item->points[0].y());
        }
        if ((item->type == DRAWRECTMODE)||(item->type == DRAWELLIPSEMODE)) {
            if (item->points[0].x() > item->points[1].x()) {qreal q = item->points[0].x(); item->points[0].setX(item->points[1].x()); item->points[1].setX(q);}
            if (item->points[0].y() > item->points[1].y()) {qreal q = item->points[0].y(); item->points[0].setY(item->points[1].y()); item->points[1].setY(q);}
            l = item->points[0].x(); t = item->points[0].y();
            w = item->points[1].x()-item->points[0].x(); h = item->points[1].y()-item->points[0].y();
        }
        if ((item->type == DRAWDOTMODE)||(item->type == DRAWCIRCLEMODE)) {
            l = item->points[0].x()- item->rad; t = item->points[0].y()- item->rad;
            w = h = item->rad*2;
        }
        if (item->type == DRAWTEXTMODE) {
           l = item->points[0].x(); t = item->points[0].y();
           w = item->item->boundingRect().width(); h = item->item->boundingRect().height();
        }
        item->bounding = QRectF(l-item->pensz/2,t-item->pensz/2,w+item->pensz,h+item->pensz);
    }
}

QGraphicsItem *MainView::drawCurrentFigure(QPointF p) {
    clearDrawing();
    drawingItem.item = 0;
    drawingItem.points.append(p);
    draw->setSnapFactor(snapFactor);
    draw->drawFigure(&drawingItem, mainPen, mainBrush);
    drawingItem.points.removeLast();
    return drawingItem.item;
}

int MainView::figureIsDone() {
    int res = 0;
    if (drawingItem.type == DRAWDOTMODE) res = 1;
    else if (drawingItem.type == DRAWLINEMODE) {
        if (drawingItem.points.count() >= 2) res = 1;
    }
    else if (drawingItem.type == DRAWRECTMODE) {
        if (drawingItem.points.count() >= 2) res = 1;
    }
    else if (drawingItem.type == DRAWCIRCLEMODE) {
        if (drawingItem.points.count() >= 2) res = 1;
    }
    else if (drawingItem.type == DRAWELLIPSEMODE) {
        if (drawingItem.points.count() >= 2) res = 1;
    }
    else if (drawingItem.type == DRAWPOLYGONMODE) {
        if ((abs(drawingItem.points[0].x()-drawingItem.points[drawingItem.points.count()-1].x())<snapFactor)&&
            (abs(drawingItem.points[0].y()-drawingItem.points[drawingItem.points.count()-1].y())<snapFactor)) {
            res = 1;
        }
    }
    else if (drawingItem.type == DRAWPOLYLINEMODE) {
        if ((abs(drawingItem.points[0].x()-drawingItem.points[drawingItem.points.count()-1].x())<snapFactor)&&
            (abs(drawingItem.points[0].y()-drawingItem.points[drawingItem.points.count()-1].y())<snapFactor)) {
            res = 1;
        }
        if (drawingItem.points.count()>1) {
            if ((abs(drawingItem.points[drawingItem.points.count()-2].x()-drawingItem.points[drawingItem.points.count()-1].x())<snapFactor)&&
                (abs(drawingItem.points[drawingItem.points.count()-2].y()-drawingItem.points[drawingItem.points.count()-1].y())<snapFactor)) {
                res = 2;
            }
        }
    }
    else if (drawingItem.type == DRAWTEXTMODE) res = 1;
    return res;
}

int MainView::figureCanComplete() {
    int res = 0;
    if (drawingItem.type == DRAWDOTMODE) res = 0;
    else if (drawingItem.type == DRAWLINEMODE) {
        if (drawingItem.points.count() >= 1) res = 1;
    }
    else if (drawingItem.type == DRAWRECTMODE) {
        if (drawingItem.points.count() >= 1) res = 1;
    }
    else if (drawingItem.type == DRAWCIRCLEMODE) {
        if (drawingItem.points.count() >= 1) res = 1;
    }
    else if (drawingItem.type == DRAWELLIPSEMODE) {
        if (drawingItem.points.count() >= 1) res = 1;
    }
    else if (drawingItem.type == DRAWPOLYGONMODE) {
        if (drawingItem.points.count() >= 2) res = 1;
    }
    else if (drawingItem.type == DRAWPOLYLINEMODE) {
        if (drawingItem.points.count() >= 2) res = 1;
    }
    if (drawingItem.type == DRAWTEXTMODE) res = 0;
    return res;
}

void MainView::newAction() {
    if (scene.list.count()) {
        foreach(SceenItem itm, scene.list) {
            drawScene->removeItem(itm.item);
        }
        model->clearSceen();
        drawedItem.clear();
        currentIndex = -1;
        if (itemSelection) {
            drawScene->removeItem(itemSelection);
            delete itemSelection;
            itemSelection = 0;
        }
    }
}

void MainView::saveAction() {
    QFile file;
    QDomDocument doc;
    QDomElement svg, gr, e;
    QDomText txt;
    QString pn;
    QString fileName = QFileDialog::getSaveFileName(this, "Save picture", 0, "*.svg");
    if (!fileName.isEmpty()) {
        if (fileName.indexOf('.')<0) fileName = fileName + ".svg";
        svg = doc.createElement("svg");
        svg.setAttribute("width", drawScene->width());
        svg.setAttribute("height", drawScene->height());
        doc.appendChild(svg);
        gr = doc.createElement("g");
        QFileInfo info(fileName);
        pn = info.fileName();
        while (pn.indexOf('.')>0) pn.remove(pn.indexOf('.'), pn.length()-pn.indexOf('.'));
        gr.setAttribute("id", pn);
        svg.appendChild(gr);
        e = doc.createElement("title");
        txt = doc.createTextNode(pn);
        e.appendChild(txt);
        gr.appendChild(e);
        int i = 0;
        foreach(SceenItem itm, scene.list) {
            if (itm.type == DRAWDOTMODE) {
                e = doc.createElement("circle");
                e.setAttribute("fill", colorToHex(itm.pen));
                e.setAttribute("stroke-width", 1);
                itm.solid = 2; itm.pensz = 1;
                e.setAttribute("cx",itm.points[0].x());
                e.setAttribute("cy",itm.points[0].y());
                e.setAttribute("r",itm.rad);
            }
            else if (itm.type == DRAWLINEMODE) {
                e = doc.createElement("line");
                e.setAttribute("x1",itm.points[0].x());
                e.setAttribute("y1",itm.points[0].y());
                e.setAttribute("x2",itm.points[1].x());
                e.setAttribute("y2",itm.points[1].y());
            }
            else if (itm.type == DRAWRECTMODE) {
                e = doc.createElement("rect");
                //e.setAttribute("stroke-opacity", "0");//without border
                e.setAttribute("x",itm.points[0].x());
                e.setAttribute("y",itm.points[0].y());
                e.setAttribute("width",itm.points[1].x()-itm.points[0].x());
                e.setAttribute("height",itm.points[1].y()-itm.points[0].y());
            }
            else if (itm.type == DRAWCIRCLEMODE) {
                e = doc.createElement("circle");
                //e.setAttribute("stroke-opacity", "0");//without border
                e.setAttribute("cx",itm.points[0].x());
                e.setAttribute("cy",itm.points[0].y());
                e.setAttribute("r",itm.rad);
            }
            else if (itm.type == DRAWELLIPSEMODE) {
                e = doc.createElement("ellipse");
                //e.setAttribute("stroke-opacity", "0");//without border
                e.setAttribute("cx",(itm.points[1].x()+itm.points[0].x())/2.0);
                e.setAttribute("cy",(itm.points[1].y()+itm.points[0].y())/2.0);
                e.setAttribute("rx",(itm.points[1].x()-itm.points[0].x())/2.0);
                e.setAttribute("ry",(itm.points[1].y()-itm.points[0].y())/2.0);
            }
            else if (itm.type == DRAWPOLYGONMODE) {
                e = doc.createElement("polygon");
                e.setAttribute("stroke-linejoin", "round");
                e.setAttribute("stroke-linecap", "round");
                pn = "";
                foreach(QPointF p, itm.points) {
                    pn = pn + QString("%1,%2 ").arg(p.x()).arg(p.y());
                }
                e.setAttribute("points",pn);
            }
            else if (itm.type == DRAWPOLYLINEMODE) {
                e = doc.createElement("polyline");
                e.setAttribute("stroke-linejoin", "round");
                e.setAttribute("stroke-linecap", "round");
                pn = "";
                foreach(QPointF p, itm.points) {
                    pn = pn + QString("%1,%2 ").arg(p.x()).arg(p.y());
                }
                e.setAttribute("points",pn);
            }
            else if (itm.type == DRAWTEXTMODE) {
                e = doc.createElement("text");
                QFontMetrics fm(itm.font);
                e.setAttribute("x",itm.points[0].x()+(itm.bounding.width()-fm.boundingRect(itm.text).width()));
                e.setAttribute("y",itm.points[0].y()+fm.ascent()+(itm.bounding.height()-fm.boundingRect(itm.text).height()));
                e.setAttribute("fill", colorToHex(itm.pen));
                e.setAttribute("stroke-width", 0);
                itm.solid = 2; itm.pensz = 0;
                if (itm.font.bold()) e.setAttribute("font-weight","bolder");
                e.setAttribute("font-family", itm.font.family());
                if (itm.font.pixelSize()==-1) e.setAttribute("font-size", QString("%1pt").arg(itm.font.pointSizeF()));
                else e.setAttribute("font-size", QString("%1px").arg(itm.font.pixelSize()));
                QDomElement e1 = doc.createElement("tspan");
                txt = doc.createTextNode(itm.text);
                qDebug() << itm.points[0];
                qDebug() << itm.bounding;
                qDebug() << fm.leading();
                qDebug() << fm.ascent();
                qDebug() << fm.descent();
                qDebug() << fm.boundingRect(itm.text);
                qDebug() << fm.leftBearing(itm.text[0]);
                e1.appendChild(txt);
                e.appendChild(e1);
            }

            //< font-size="16px"><tspan>Привет</tspan></text>

            if (itm.solid==1) e.setAttribute("fill", colorToHex(itm.brush));
            else if (itm.solid==0) e.setAttribute("fill", "none");
            e.setAttribute("stroke", colorToHex(itm.pen));
            e.setAttribute("stroke-width", itm.pensz);
            e.setAttribute("id",itm.name);
            gr.appendChild(e);
            i++;
        }

        file.setFileName(fileName);
        file.open(QFile::WriteOnly);
        file.write(QString("<?xml version=\"1.0\" standalone=\"no\"?>\n").toUtf8());
        file.write(doc.toByteArray());
        file.close();
    }
}

void MainView::openAction() {
    QFile file;
    QByteArray b;
    QDomDocument doc;
    QDomElement svg, gr, e;
    QString pn, id, title, nn;
    int width, height, type;
    QPointF p;
    SceenItem itm;
    qreal cx,cy,rx,ry;
    QHash <QString, int> nodes;
    QStringList names, ls; names << "" << "dot" << "line" << "rect" << "circle" << "ellipse" << "polygon" << "poliline" << "text";
    QString fileName = QFileDialog::getOpenFileName(this, "Open picture", 0, "*.svg");
    nodes.insert("circle", DRAWCIRCLEMODE);
    nodes.insert("line", DRAWLINEMODE);
    nodes.insert("rect", DRAWRECTMODE);
    nodes.insert("ellipse", DRAWELLIPSEMODE);
    nodes.insert("polygon", DRAWPOLYGONMODE);
    nodes.insert("polyline", DRAWPOLYLINEMODE);
    nodes.insert("text", DRAWTEXTMODE);
    nodes.insert("title", 100);
    if (!fileName.isEmpty()) {
        if (fileName.indexOf('.')<0) fileName = fileName + ".svg";
        file.setFileName(fileName);
        file.open(QFile::ReadOnly);
        newAction();
        b = file.readAll();
        doc.setContent(b);
        file.close();

        svg = doc.firstChildElement("svg");
        width = svg.attribute("width").toInt();
        height = svg.attribute("height").toInt();

        gr = svg.firstChildElement("g");
        id = gr.attribute("id");
        e = gr.firstChildElement("title");
        title = e.nodeValue();
        int i = 0;
        e = gr.firstChildElement();
        while (!e.isNull()) {
            nn = e.nodeName();
            type = nodes[nn.toLower()];
            itm.type = type;
            itm.pen = QColor(e.attribute("stroke", "#000000"));
            pn = e.attribute("fill","none");
            if (!pn.compare("none", Qt::CaseInsensitive)) {itm.solid = 0; pn = "#000000";}
            else itm.solid = 1;
            itm.brush = QColor(pn);
            itm.pensz = e.attribute("stroke-width", "1").toInt();
            itm.rad = e.attribute("r","0").toInt();
            if ((itm.type>=0)&&(itm.type<=DRAWMODECOUNT)) {
                itm.name = e.attribute("id",names[itm.type] + QString("_%1").arg(i));
                itm.points.clear();
                switch(type) {
                case DRAWLINEMODE:
                    itm.points.append(QPointF(e.attribute("x1","0").toDouble(),(e.attribute("y1","0").toDouble())));
                    itm.points.append(QPointF(e.attribute("x2","0").toDouble(),(e.attribute("y2","0").toDouble())));
                    break;
                case DRAWRECTMODE:
                    p = QPointF(e.attribute("x","0").toDouble(),(e.attribute("y","0").toDouble()));
                    itm.points.append(p);
                    p.setX(p.x()+e.attribute("width","0").toDouble());
                    p.setY(p.y()+e.attribute("height","0").toDouble());
                    itm.points.append(p);
                    break;
                case DRAWCIRCLEMODE:
                    p = QPointF(e.attribute("cx","0").toDouble(),(e.attribute("cy","0").toDouble()));
                    itm.points.append(p);
                    p.setX(p.x()+itm.rad);
                    itm.points.append(p);
                    break;
                case DRAWELLIPSEMODE:
                    cx = e.attribute("cx","0").toDouble(); cy = e.attribute("cy","0").toDouble();
                    rx = e.attribute("rx","0").toDouble(); ry = e.attribute("ry","0").toDouble();
                    itm.points.append(QPointF(cx-rx,cy-ry));
                    itm.points.append(QPointF(cx+rx,cy+ry));
                    break;
                case DRAWPOLYGONMODE:
                    //e.attribute("stroke-linejoin", "round");
                    //e.attribute("stroke-linecap", "round");
                    pn = e.attribute("points","");
                    ls = pn.split(" ");
                    foreach(pn, ls) {
                        if (!pn.isEmpty()) {
                            QStringList lst = pn.split(',');
                            if (lst.count() > 1) {
                                p.setX(lst[0].toDouble());
                                p.setY(lst[1].toDouble());
                                itm.points.append(p);
                            }
                        }
                    }
                    break;
                case DRAWPOLYLINEMODE:
                    //e.attribute("stroke-linejoin", "round");
                    //e.attribute("stroke-linecap", "round");
                    pn = e.attribute("points","");
                    ls = pn.split(" ");
                    foreach(pn, ls) {
                        if (!pn.isEmpty()) {
                            QStringList lst = pn.split(',');
                            if (lst.count() > 1) {
                                p.setX(lst[0].toDouble());
                                p.setY(lst[1].toDouble());
                                itm.points.append(p);
                            }
                        }
                    }
                    break;
                case DRAWTEXTMODE:
                    pn = e.attribute("font-weight","");
                    if (!pn.isEmpty()) itm.font.setBold(true);
                    else itm.font.setBold(false);
                    pn = e.attribute("font-family", "");
                    if (!pn.isEmpty()) itm.font.setFamily(pn);
                    pn = e.attribute("font-size", "12pt");
                    if (pn[pn.count()-1] == 't') {
                        pn.remove(pn.count()-2,2);
                        itm.font.setPointSize(pn.toDouble());
                    }
                    else if (pn[pn.count()-1] == 'x') {
                        pn.remove(pn.count()-2,2);
                        itm.font.setPixelSize(pn.toDouble());
                    }
                    else itm.font.setPointSize(12);
                    QDomElement e1 = e.firstChildElement("tspan");
                    itm.text = e1.text();

                    QFontMetrics fm(itm.font);
                    p = QPointF(e.attribute("x","0").toDouble(),(e.attribute("y","0").toDouble()));
                    itm.points.append(p);
                    draw->drawFigure(&itm,QPen(),QBrush());
                    updateBoundingRect(&itm);
                    itm.points.clear();
                    drawScene->removeItem(itm.item);
                    delete itm.item;
                    p.setX(p.x()-itm.bounding.width()+fm.boundingRect(itm.text).width());
                    p.setY(p.y()-fm.ascent()-itm.bounding.height()+fm.boundingRect(itm.text).height());
                    itm.points.append(p);
                    break;
                }
                QBrush brush;
                if (itm.solid) brush = QBrush(itm.brush);
                else brush = QBrush();
                draw->drawFigure(&itm,QPen(itm.pen,itm.pensz),brush);
                updateBoundingRect(&itm);
                model->addFigure(itm, false);
                drawedItem.append(itm.item);
                i++;
            }
            e = e.nextSiblingElement();
        }
       /* foreach(SceenItem itm, scene.list) {
            if (itm.type == DRAWDOTMODE) {
                e = doc.createElement("circle");
                e.setAttribute("fill", colorToHex(itm.pen));
                e.setAttribute("stroke-width", 1);
                itm.solid = 2; itm.pensz = 1;
                e.setAttribute("cx",itm.points[0].x());
                e.setAttribute("cy",itm.points[0].y());
                e.setAttribute("r",itm.rad);
            }
         }*/
        fileOptions.height = height;
        fileOptions.width = height;
        updateOptions();
    }
}

QString MainView::colorToHex(QColor color) {
    QRgb rgb = color.rgba();
    QString str = "";
    QString hex="0123456789ABCDEF";
    for(int i = 0; i < 6; i++) {
        str = hex[rgb&0xF] + str;
        rgb = rgb >> 4;
    }
    str = QString('#') + str;
    return str;
}

void MainView::completeAction() {
    if (drawingItem.type == DRAWPOLYGONMODE) {
        curPoint.setX(drawingItem.points[0].x());
        curPoint.setY(drawingItem.points[0].y());
    }
    if (drawingItem.type == DRAWPOLYLINEMODE) {
        curPoint.setX(drawingItem.points[drawingItem.points.count()-1].x());
        curPoint.setY(drawingItem.points[drawingItem.points.count()-1].y());
    }
    QGraphicsItem *it = drawCurrentFigure(curPoint);
    drawingItem.points.append(curPoint);
    if (figureIsDone()) drawDone(it);
}

void MainView::cancelAction() {
    clearDrawing();
    drawDone(0);
}

void MainView::propertyAction() {
    if (currentIndex >= 0) {
        FigureProperties *prop = new FigureProperties(this);
        prop->setFigure(scene.list.at(currentIndex));
        if (prop->exec()) {
            SceenItem itm = prop->figure();
            drawScene->removeItem(scene.list[currentIndex].item);
            delete scene.list[currentIndex].item;
            QBrush brush;
            if (itm.solid) brush = QBrush(itm.brush);
            else brush = QBrush();
            draw->drawFigure(&itm,QPen(itm.pen,itm.pensz),brush);
            scene.list[currentIndex] = itm;
            model->updateFigureData(currentIndex);
        }
        delete prop;
    }
}

void MainView::removeAction() {
    if (scene.list.count()) {
        if (itemSelection&&(currentIndex>=0)) {
            drawScene->removeItem(scene.list[currentIndex].item);
            model->removeItem(currentIndex);
            drawedItem.removeAt(currentIndex);
            currentIndex = -1;
            drawScene->removeItem(itemSelection);
            delete itemSelection;
            itemSelection = 0;
        }
    }
}

void MainView::showMenu(QVector<int> actions, QPoint pos) {
    int i, j = 0;
    QMenu menu;
    QAction **action, *selAction;
    if (!hasMenu&&(actions.count() > 0)) {
        action = new QAction*[actions.count()];
        foreach (i, actions) {
            action[j] = new QAction(actionList[i].name, this);
            action[j]->setProperty("signalId", actionList[i].signal);
            if (actionList[i].signal == 0) if (!figureCanComplete()) action[j]->setEnabled(false);
            menu.addAction(action[j]);
            j++;
        }
        hasMenu = 1;
        selAction = menu.exec(pos);
        if (selAction) {
            i = selAction->property("signalId").toInt();
            QMetaObject::activate(this,&staticMetaObject, i, NULL);
        }
        for (i = 0; i < actions.count(); i++) delete action[i];
        delete action;
        hasMenu = 0;
    }
}

void MainView::setGridSize(int size) {
    GridSize = size;
    if (ShowGrid) drawGrid(3);
}

void MainView::setShowGrid(bool show) {
    ShowGrid = show;
    if (show) drawGrid(2);
    else drawGrid(1);
}

void MainView::drawGrid(int flag) {
    QGraphicsItem *itm;
    QList <QGraphicsItem*> gr;
    QPen pen = QPen(QColor(0xF3,0xF3,0xF3), gridPenWidth);
    if (flag&1) {
        if (gridElement) {
            drawScene->removeItem(gridElement);
            delete gridElement;
        }
    }
    if (flag&2) {
        for (int i = 0; i < drawScene->width(); i+= GridSize) {
            itm = drawScene->addLine(i, 0, i, drawScene->height()-1, pen);
            itm->setZValue(-1);
            gr.append(itm);
        }
        for (int i = 0; i < drawScene->height(); i+= GridSize) {
            itm = drawScene->addLine(0, i, drawScene->width()-1, i, pen);
            itm->setZValue(-1);
            gr.append(itm);
        }
        gridElement = drawScene->createItemGroup(gr);
    }
}

void MainView::drawSelection(int flag) {
    qreal selZ = 100, sz2 = 2*gridPenWidth, sz = 2*sz2;
    QPen pen;
    QBrush brush(QColor(0x1D,0x8D,0x1D));
    pen.setColor(QColor(0x5D,0x5D,0x5D));
    pen.setWidthF(gridPenWidth);
    pen.setStyle(Qt::DashLine);
    if (itemSelection) {
        drawScene->removeItem(itemSelection);
        delete itemSelection;
    }
    if (itemSelection||flag) {
        QGraphicsItem *itm;
        QList <QGraphicsItem*> gr;
        QRectF r = scene.list.at(currentIndex).bounding;
        itm = drawScene->addRect(r, pen);
        gr.append(itm);
        pen.setStyle(Qt::SolidLine);
        itm = drawScene->addRect(r.left()-sz2, r.top()-sz2, sz, sz, pen, brush);
        gr.append(itm);
        itm = drawScene->addRect(r.left()+r.width()-sz2, r.top()-sz2, sz, sz, pen, brush);
        gr.append(itm);
        itm = drawScene->addRect(r.left()-sz2, r.top()+r.height()-sz2, sz, sz, pen, brush);
        gr.append(itm);
        itm = drawScene->addRect(r.left()+r.width()-sz2, r.top()+r.height()-sz2, sz, sz, pen, brush);
        gr.append(itm);
        itemSelection = drawScene->createItemGroup(gr);
        itemSelection->setZValue(selZ);
    }
}

void MainView::setSnapToGrid(bool snap) {
    SnapToGrid = snap;
}

void MainView::clearDrawing() {
    if (drawingItem.item) {
        drawScene->removeItem(drawingItem.item);
        delete drawingItem.item;
        drawingItem.item = 0;
    }
}

void MainView::aproximate() {
    QVector<QPointF> pnt;
    QPointF p;
    foreach(SceenItem itm, scene.list) {
        if (itm.type == DRAWDOTMODE) {
            p.setX(itm.points[0].x());
            p.setY(itm.points[0].y());
            pnt.append(p);
        }
    }

    approxParam par, par1;
    calcApprox(pnt, &par);
    pnt.removeAt(par.max_ni);
    calcApprox(pnt, &par1);
    while (par1.sum_n/par.sum_n < 0.75) {
        par = par1;
        pnt.removeAt(par.max_ni);
        calcApprox(pnt, &par1);
        qDebug() << par.sum_n << par1.sum_n << par1.sum_n/par.sum_n;

    }

    drawingItem.type = DRAWLINEMODE;
    drawingItem.points.clear();
    drawingItem.name = "approx";
    p.setX(par.minx);
    p.setY(par.a*par.minx+par.b);
    drawingItem.points.append(p);
    p.setX(par.maxx);
    p.setY(par.a*par.maxx+par.b);
    drawingItem.points.append(p);
    draw->drawFigure(&drawingItem, mainPen, mainBrush);

    drawDone(drawingItem.item);
}

void MainView::calcApprox(QVector<QPointF> p, approxParam *param) {
    int i, n = p.count();
    double sx = 0, sqx = 0, sy = 0, sxy = 0;
    param->maxx=param->minx=p.at(0).x();
    for(i = 0; i < n; i++) {
        sx += p.at(i).x();
        sy += p.at(i).y();
        sqx += pow(p.at(i).x(),2);
        sxy += p.at(i).x()*p.at(i).y();
        if (param->minx>p.at(i).x()) param->minx = p.at(i).x();
        if (param->maxx<p.at(i).x()) param->maxx = p.at(i).x();
    }

    param->a = (n*sxy-sx*sy)/(n*sqx-sx*sx);
    param->b = (sy-param->a*sx)/n;
    param->sum_n = 0;
    param->max_n = fabs(p.at(0).y()-(param->a*p.at(0).x()+param->b));
    param->max_ni = 0;
    double nn;
    for(i = 0; i < n; i++) {
        nn = fabs(p.at(i).y()-(param->a*p.at(i).x()+param->b));
        param->sum_n += nn;
        if (nn>param->max_n) {param->max_n = nn; param->max_ni = i;}
    }
    param->sum_n /= n;
}
