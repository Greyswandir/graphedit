#ifndef FIGUREPROPERTIES_H
#define FIGUREPROPERTIES_H

#include <QDialog>
#include "figuremodel.h"
#include "drawfigure.h"

namespace Ui {
class FigureProperties;
}

class FigureProperties : public QDialog
{
    Q_OBJECT

public:
    explicit FigureProperties(QWidget *parent = 0);
    ~FigureProperties();
    void setFigure(SceenItem itm);
    SceenItem figure();
    QIcon iconFromColor(QColor color);


private slots:
    void on_solidButton_clicked();
    void on_mainColorButton_clicked();
    void on_backColorButton_clicked();
    void on_sizeList_currentTextChanged(const QString &arg1);

    void on_figureName_textChanged(const QString &arg1);

private:
    void drawFigure();

    Ui::FigureProperties *ui;
    SceenItem sceneFigure;
    QGraphicsScene *mainScene;
    DrawFigure *draw;
    QGraphicsItem *mainFigure;
};

#endif // FIGUREPROPERTIES_H
