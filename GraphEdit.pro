#-------------------------------------------------
#
# Project created by QtCreator 2015-02-19T15:53:14
#
#-------------------------------------------------

QT       += core gui xml svg

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GraphEdit
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
        mainview.cpp \
        figuremodel.cpp \
    textdialog.cpp \
    figureproperties.cpp \
    drawfigure.cpp \
    fileoptionsdialog.cpp

HEADERS  += mainwindow.h \
        graphConst.h \
        mainview.h \
        figuremodel.h \
    textdialog.h \
    figureproperties.h \
    drawfigure.h \
    fileoptionsdialog.h

FORMS    += mainwindow.ui \
    textdialog.ui \
    figureproperties.ui \
    fileoptionsdialog.ui

RESOURCES += \
    resource.qrc
