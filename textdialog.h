#ifndef TEXTDIALOG_H
#define TEXTDIALOG_H

#include <QDialog>

namespace Ui {
class TextDialog;
}

class TextDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TextDialog(QWidget *parent = 0);
    ~TextDialog();
    QString enteredText();
    QFont currentFnt();

private slots:
    void on_fontButton_clicked();

private:
    Ui::TextDialog *ui;
    QFont curFont;
};

#endif // TEXTDIALOG_H
